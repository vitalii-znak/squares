'use strict'
/*POLIFILLS*/
const reduce = Function.bind.call(Function.call, Array.prototype.reduce);
const isEnumerable = Function.bind.call(Function.call, Object.prototype.propertyIsEnumerable);
const concat = Function.bind.call(Function.call, Array.prototype.concat);
const keys = Reflect.ownKeys;
if (!Object.values) {
    Object.values = (O) => reduce(keys(O), (v, k) => concat(v, typeof k === 'string' && isEnumerable(O, k) ? [O[k]] : []), []);
}
if (!Object.entries) {
    Object.entries = (O) => reduce(keys(O), (e, k) => concat(e, typeof k === 'string' && isEnumerable(O, k) ? [
        [k, O[k]]
    ] : []), []);
}
/*END POLIFILL*/


const WorldMap = {};
const turnHistory = [];

const dimensions = [6, 6],
    colorsNum = 7,
    squareColorClasses = ['green', 'red', 'yellow', 'blue', 'purple', 'brown', 'orange'];

const MAX_VIEWPORT_WIDTH = 550,
    width = window.innerWidth < MAX_VIEWPORT_WIDTH ? window.innerWidth - 20 : MAX_VIEWPORT_WIDTH - 20;

const playgroundMaxWidth = parseInt($('#viewWrapper').css('max-width'));
console.log(playgroundMaxWidth);
const $squareTmpl = $('#squareTmpl').find('.square_wrapper'),
    $playground = $('#playGound').css({
        width: width,
        height: width * (dimensions[0] / dimensions[1]),
        'max-width': playgroundMaxWidth,
        'max-height': playgroundMaxWidth * (dimensions[0] / dimensions[1])
    })
const containerDimensions = [$playground.height(), $playground.width()]
$('.scoreView').css('width', width);


//almost LISP :)
/* detect continuous color chain */
function continuous(world, pos, color) {
    const mem = [];
    return Object.values(
        function continuousAux(world, pos, color = undefined) {
            const colorAux = (color !== undefined ? color : world[pos]);
            if (world[pos] === undefined ||
                colorAux === undefined ||
                mem[pos] !== undefined ||
                world[pos] !== colorAux)
                return mem;
            else {
                mem[pos] = pos;
                return $.extend({},
                    continuousAux(world, [pos[0] - 1, pos[1]], colorAux),
                    continuousAux(world, [pos[0] + 1, pos[1]], colorAux),
                    continuousAux(world, [pos[0], pos[1] - 1], colorAux),
                    continuousAux(world, [pos[0], pos[1] + 1],
                        colorAux
                    ));
            }
            return result;
        }(world, pos, color));
}

function nextCell(world) {
    for (let key in world) {
        if (world[key] === null) {
            let pos = JSON.parse('[' + key + ']')
            return pos;
        }
    }
    return null;
}


const transpose = m => m[0].map((x, i) => m.map(x => x[i]));

function colorTree(colors) {
    let toReturn;
    let i = 0;

    let AllPos;
    do {
        let startTime = Date.now();
        let world = {};
        for (let rowInt = 0; rowInt < dimensions[0]; rowInt++) {
            for (let columnInt = 0; columnInt < dimensions[1]; columnInt++) {
                world[[rowInt, columnInt]] = null;
            }
        }
        let color = getRandomInt(0, colors.length);
        world[[0, 0]] = color;
        let node = {
            pos: [0, 0],
            parrent: null,
            world: world,
            children: {}
        }

        let newPos = nextCell(node.world);
        let newWorld = $.extend(true, {}, node.world);
        let colorsAvailable = colors.filter(el => !Object.keys(node.children).includes(el));

        newWorld[newPos] = colorsAvailable[getRandomInt(0, colorsAvailable.length)];
        node.children[newWorld[newPos]] = {
            pos: newPos,
            parrent: node,
            world: newWorld,
            children: {}
        }
        AllPos = [];
        try {
            toReturn = (function colorTreeAux(node, colors) {
                if (Date.now() - startTime > 1000) {
                    throw "too long";
                }

                const colorsAvailable = colors.filter(el => !Object.keys(node.children).includes(el));
                if (!colorsAvailable.length) return colorTreeAux(node.parrent, colors);
                if (continuous(node.world, node.pos).length > 2) {
                    //back
                    return colorTreeAux(node.parrent, colors);
                } else {
                    const newPos = nextCell(node.world);
                    AllPos.push(newPos);
                    if (!newPos) {
                        return node.world;
                    } else {
                        const newWorld = $.extend(true, {}, node.world);

                        newWorld[newPos] = colorsAvailable[getRandomInt(0, colorsAvailable.length)];
                        return colorTreeAux(
                            node.children[newWorld[newPos]] = {
                                pos: newPos,
                                parrent: node,
                                world: newWorld,
                                children: {}
                            },
                            colors);
                    }
                }
            }(node.children[newWorld[newPos]], colors));
        } catch (err) { console.log(err); }
        i++;
    } while (!toReturn && i < 20);
    return Math.round(Math.random()) ? toReturn : transposeMap(toReturn);

};

function transposeMap(worldColors) {
    const newWorldColors = {};
    for (let rowInt = 0; rowInt < dimensions[0]; rowInt++) {
        for (let columnInt = 0; columnInt < dimensions[1]; columnInt++) {
            newWorldColors[[columnInt, rowInt]] = worldColors[[rowInt, columnInt]]
        }
    }
    return newWorldColors;
}


function drawWorld(worldColors, $playground) {
    const cellsNum = dimensions[0] * dimensions[1];
    for (let rowInt = 0; rowInt < dimensions[0]; rowInt++) {
        for (let columnInt = 0; columnInt < dimensions[1]; columnInt++) {
            let type = worldColors[[rowInt, columnInt]];
            WorldMap[[rowInt, columnInt]] = {
                square: {
                    type: type,
                    active: true
                },
                $square: $squareTmpl.clone().css({
                        'top': rowInt * containerDimensions[0] / dimensions[0] + 'px',
                        'left': columnInt * containerDimensions[1] / dimensions[1] + 'px',
                        width: containerDimensions[1] / dimensions[1] + 'px',
                        height: containerDimensions[0] / dimensions[0] + 'px'
                    })
                    .addClass((type !== undefined && type !== null) ? squareColorClasses[type] : '')
                    .attr('data-pos', [rowInt, columnInt].join(';'))
                    .appendTo($playground)
            }
        }
    }
}
drawWorld(colorTree(Array.from({ length: colorsNum }, (v, k) => k)), $playground);

let controllState = 'CHANGE_COLOR';
$("[data-id='changeColorStateBtn']").on('click', () => {
    $("[data-id='eraseStateBtn']").removeClass('active');
    $("[data-id='changeColorStateBtn']").addClass('active');
    controllState = 'CHANGE_COLOR';
});
$("[data-id='eraseStateBtn']").on('click', () => {
    $("[data-id='eraseStateBtn']").addClass('active');
    $("[data-id='changeColorStateBtn']").removeClass('active');
    controllState = 'ERASE';
});

const touchCell = (function() {
    var permited = true;
    return function(ev) {
        if (!permited) return;
        permited = false;
        const pos = this.dataset.pos.split(";").map(el => parseInt(el));
        if (controllState === 'CHANGE_COLOR') changeColor(pos).then(() => permited = true)
        else erase(pos).then(() => permited = true)
    }
}());

$playground.on('click', '.square_wrapper', touchCell);


function changeColor(pos) {
    //if not cross
    //model
    if (!WorldMap[pos] || !WorldMap[pos].square.active) {
        return Promise.resolve(null);
    }
    WorldMap[pos].square.active = false;
    const colorsAvailable = Array.from({ length: colorsNum }, (v, k) => k).filter(el => el !== WorldMap[pos].square.type);
    const newColor = colorsAvailable[getRandomInt(0, colorsAvailable.length)];
    WorldMap[pos].square.type = newColor;

    //view
    WorldMap[pos].$square
        .removeClass(squareColorClasses.join(' '))
        .addClass(squareColorClasses[newColor])
        .find('svg').show();
    return Promise.resolve(newColor);
}


function erase(pos) {
    return eraseCells(pos)
        .then(updateScore)
        .then(dropWorld)
        .then(addNew);
}

function eraseCells(pos) {
    const colorsGraph = {}
    for (let prop in WorldMap) {
        colorsGraph[prop] = WorldMap[prop].square.type;
    }
    const contin = continuous(colorsGraph, pos);
    if (contin.length > 2) {
        return Promise.all(contin.map((pos) => {
            WorldMap[pos].square.type = null;
            return new Promise((resolve, reject) =>
                WorldMap[pos].$square.animate({
                    opacity: 0
                }, 300, "swing", () => {
                    resolve(pos);
                }))
        }))
    } else {
        return Promise.resolve([]);
    }
}

function updateScore(positions) {
    return Promise.resolve(positions);
}

function dropWorld(positions) {
    //fall world
    const promices = [];
    for (let row = dimensions[0] - 1; row >= 0; row--) {
        for (let col = dimensions[1] - 1; col >= 0; col--) {
            //fall every square
            if (WorldMap[[row, col]].square.type === null) continue;
            let rowNext = row;
            while ((WorldMap[[rowNext + 1, col]] &&
                    WorldMap[[rowNext + 1, col]].square &&
                    WorldMap[[rowNext + 1, col]].square.type === null) ||
                (WorldMap[[rowNext + 1, col]] === null)) rowNext++;
            if (rowNext !== row && WorldMap[[row, col]] !== undefined) {
                promices.push(new Promise(function(resolve, reject) {
                    WorldMap[[row, col]].$square
                        .attr('data-pos', [rowNext, col].join(';'))
                        .animate({
                            'top': rowNext * containerDimensions[0] / dimensions[0] + 'px'
                        }, 450, "swing", resolve);

                    //model
                    let auxCell = WorldMap[[rowNext, col]];
                    WorldMap[[rowNext, col]] = WorldMap[[row, col]];
                    WorldMap[[row, col]] = auxCell;

                    //view
                    WorldMap[[row, col]].$square.attr('data-pos', [row, col].join(';'))
                        .css({
                            'top': row * containerDimensions[0] / dimensions[0] + 'px'
                        });
                }));
            }
        }
    }
    return Promise.all(promices);
}

function addNew(positions) {
    const promises = [];
    for (let row = dimensions[0] - 1; row >= 0; row--) {
        for (let col = dimensions[1] - 1; col >= 0; col--) {
            let pos = [row, col];
            if (WorldMap[pos].square.type !== null) continue;


            //game model
            /*const colorGraphsCandidates = {}
            for (let prop in WorldMap) {
                colorsGraph[prop] = WorldMap[prop].square.type;
            }
            do{
                const contin = continuous(colorsGraph, pos);

            } while ()*/


            const colorsAvailable = Array.from({ length: colorsNum }, (v, k) => k);
            const newColor = colorsAvailable[getRandomInt(0, colorsAvailable.length)];
            WorldMap[pos].square.type = newColor;
            WorldMap[pos].square.active = true;

            //game view
            promises.push(
                new Promise(function(resolve, reject) {
                    WorldMap[pos].$square
                        .find('svg').hide().end()
                        .removeClass(squareColorClasses.join(' '))
                        .addClass(squareColorClasses[WorldMap[pos].square.type])
                        .animate({
                            'opacity': 1
                        }, 470, "swing", resolve);
                }));
        }
    }
    return Promise.all(promises);
}



//better random numbers
function getRandomInt(min, max) {
    const maxAux = max == 0 ? 0 : max - 1;
    const byteArray = new Uint8Array(1);
    crypto.getRandomValues(byteArray);

    const range = maxAux - min + 1;
    const maxRange = 256;
    return byteArray[0] >= Math.floor(maxRange / range) * range ?
        getRandomInt(min, maxAux) :
        min + (byteArray[0] % range);
}



$('[data-id="helpBtn"]').on('click', help);
$('[data-id="resumeBtn"]').on('click', resume);;

function resume() {
    console.log('resume');
}

function help() {
    console.log('help')
}


/*****************/
//  TUTORIAL
/****************/
