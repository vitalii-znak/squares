'use strict'
const express = require('express');
const app = express();

const crypto = require('crypto');
const path = require('path');
app.use('/public', express.static(path.join(__dirname, '../client')));



const http = require('http');
const server = http.createServer(app);



function Game(socket) {
    if (!(this instanceof Game)) return new Game();
    const dimensions = [4, 4],
        world = [],
        squareDirs = ['u', 'd', 'l', 'r'];
    for (var row = 0; row < dimensions[0]; ++row) {

        world[row] = [];

        for (var column = 0; column < dimensions[1]; ++column) {
            world[row].push({
                square: {
                    dir: squareDirs[Math.floor(Math.random() * squareDirs.length)],
                    type: Math.floor(Math.random()*2)
                },
                pos: [
                    [row],
                    [column]
                ],
                empty: false
            });
        }
    }
    this.socket = socket;
    this.world = world;
}

Game.prototype.act = function(coords, scores = []) {


    const cell = this.world[coords[0]][coords[1]];
    const square = cell.square;
    const posNext = cell.pos.slice();
    cell.empty = true;

    const axis = square.dir === 1 || square.dir === 2 ? 0 : 1;
    while ( this.world[square.dir === 1 || square.dir === 2 ?
            --posNext[axis] :
            ++posNext[axis]] && this.world[posNext[0]][posNext[1]] && this.world[posNext[0]][posNext[1]].empty);


    scores.push({ type: square.type, score: scores.length + 1 })
    if (this.world[posNext[0]] && this.world[posNext[0]][posNext[1]] && !this.world[posNext[0]][posNext[1]].empty) {
        this.act(posNext, scores);
    }
};

const io = require('socket.io')(server);
io.on('connection', (socket) => {

    socket.on('world', () => {
        socket.game = Game();
        socket.emit('world', {
            world: socket.game.world
        });
    })

    socket.on('act', () => {
      
        socket.game.act([
            [0], [0]
        ]);
        socket.emit('act', {
            world: socket.game.world
        });
    })


})


app.get(['/', '/:id'], function(req, res) {
    res.sendFile(path.join(__dirname, '../client/index.html'));

})

server.listen(3000, function() {
    console.log('listening on *:3000');
})
